import settings
import sys

import settings
from publish import Publisher

if sys.argv.__len__() != 3:
    print('Images folder needed and text message needed')
    exit(1)

IMAGES_FOLDER = sys.argv[1]
TEXT = sys.argv[2]

publisher = Publisher(IMAGES_FOLDER)

publisher.publish(tlg_msg=TEXT, twt_msg=TEXT)
