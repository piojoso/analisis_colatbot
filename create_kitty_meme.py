"""
Creates a meme if at least N alerts have been published to the memetro.org app on a day
"""
import json, urllib.request, sys, requests
import os
import time
import random
import settings
import logging
from PIL import Image, ImageDraw, ImageFont

logging.basicConfig(filename='kitty.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

if len(sys.argv) != 3: sys.exit("Usage: python3 cat_meme.py THRESHOLD OUT_DIR")

THRESHOLD = sys.argv[1]
OUTPUT_FOLDER = sys.argv[2]
MEMETRO_ENDPOINT = "https://memetro.org:8082/alerts/?daysAgo=1"

# Load the alerts from the past N days in memory
with urllib.request.urlopen(MEMETRO_ENDPOINT) as url:
    list_alerts = json.load(url)
reveived_alerts = len(list_alerts)

# Finish the script if the threshold is not reached
if reveived_alerts < int(THRESHOLD):
    logging.info(f"Threshold NOT met: {reveived_alerts} alerts received")
    logging.info(f"Threshold: {THRESHOLD}")
    sys.exit("Threshold not met")

logging.info(f"Threshold met: {reveived_alerts} alerts received")
logging.info(f"Threshold: {THRESHOLD}")


def random_msg():
    lines = open(settings.KITTY_PHRASES).read().splitlines()
    line = random.choice(lines)
    return line.replace('\\n', '\n')


def add_text_to_image(image_path, bottom_text):
    img = Image.open(image_path)
    imW, imH = img.size
    draw = ImageDraw.Draw(img)
    font="font/font.ttf"
    font_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), font)
    font_title = ImageFont.truetype(font_path, int(imH / 15))
    font_sub_title = ImageFont.truetype(font_path, int(imH / 25))
    font_text = ImageFont.truetype(font_path, int(imH / 25))

    top = "MEMETRO INFORMA"
    greets = f"Gràcies a les vostres {reveived_alerts} alertes d'avui"

    _, _, tw, th = draw.textbbox((0, 0), top, font=font_title,)
    _, _, b1w, b1h = draw.textbbox((0, 0), greets, font=font_sub_title)
    _, _, b2w, b2h = draw.textbbox((0, 0), bottom_text, font=font_text)
    draw.text(((imW - tw) / 2, (10)), top, font=font_title, fill="white")
    draw.text(((imW - b1w) / 2, (10 + th + 10)), greets, font=font_sub_title, fill="white")
    draw.text(((imW - b2w) / 2, (imH - b2h - 10)), bottom_text, font=font_text, fill="white")

    # Save the edited image
    img.save(image_path)


if not os.path.exists(OUTPUT_FOLDER):
    os.makedirs(OUTPUT_FOLDER)

# If the threshold is met, download the meme:
# MEME_MESSAGE = f"Memetro informa:\nGràcies a les vostres {reveived_alerts} alertes d'avui\nhem salvat aquest gatet d'un control!"
# prefix = f"MEMETRO INFORMA\nGràcies a les {reveived_alerts} alertes d'avui\n"
# prefix = f"Gràcies a les {reveived_alerts} alertes d'avui "
# MEME_MESSAGE = prefix + random_msg()
# CAT_MEME_SOURCE = f"https://cataas.com/cat/cute/says/{MEME_MESSAGE}"
CAT_MEME_SOURCE = f"https://cataas.com/cat/cute"
CAT_FILE = OUTPUT_FOLDER + '/cat_meme.jpg'

success = False
while (True):  # Get a meme at least 450pixel wide
   img_data = requests.get(CAT_MEME_SOURCE).content

   with open(OUTPUT_FOLDER + '/cat_meme.jpg', 'wb') as handler:
       handler.write(img_data)

   cat_image = Image.open(OUTPUT_FOLDER + '/cat_meme.jpg')
   if cat_image.width >= 450:
       success = True
       break
   else:
       time.sleep(1)

if success:
    add_text_to_image(CAT_FILE, random_msg())
    print("Meme generated")
