#Petit anàlisis de les dades publicades a t.me/AvisosControlsBCNCanal.
#Aquest script s'ha d'executar després d'haver executat el programa colatbot_html2json.py
#Aquest script carrega en memòria tots els avisos generats pel programa anterior (que es troben
#enmagatzemats en els fitxers .json) i crea unes poques dades estadístiques i gràfiques.
    #Les dades tenen la seguent estructura:
    #        "dia": "24.09.2021",
    #        "hora": "11:08:07",
    #        "tipus": "Lliure",
    #        "transport": "Metro",
    #        "linea": "L3",
    #        "parada": "Canyelles",
    #        "observacions": "No"

from os import listdir
import json
import datetime
import matplotlib.pyplot as plt
from scipy import stats
from math import ceil

llista_avisos = [] #Tots els avisos de tots els fitxers

#Llista de tots els fitxers .json que es volen analitzar, en ordre temporal
input_files = ["messages.json", "messages2.json", "messages3.json"]

#Per cada fitxer .json, carrega les dades i afageix-les a una sola variable
for file in input_files:
    with open(file, 'r') as infile:
        llista_avisos.extend(json.load(infile)) #Afegeix la llista d'alertes

################ Eines per a estudiar les dades ################################
class control_mosquits:
  def __init__(self, inici, final, parada, linea, transport, alertes):
    self.inici = inici
    self.final = final
    self.parada = parada
    self.linea = linea
    self.transport = transport
    self.alertes = alertes #Num d'alertes que ha rebut aquest control de Mosquits
    self.controlActiu = True #Ha finalitzat el control de mosquits? o encara hi son?

''' def avisos2controlsMosquits(llista_tots_avisos):
    """Parteix d'una llista de tots els avisos registrats i
    crea una llista de tots els controlls de "Mosquis" trobats.
    Per a discernir quines incidències formen part del mateix control, i quines
    formen par d'un control diferent, es fan servir les següents definicions:
    *Inici d’una incidència de «Mosquits»: Moment en el que es rep la primera alerta d’aquesta.
    *Final d’una incidència de «Mosquits»: Quan es rep una alerta de «Via Lliure»
        en la mateixa estació on va començar la incidència o quan han passat 6h
        de la primera alerta. Si després de rebre la «Via Lliure» es rep una nova
        alerta de «Mosquits» en un temps inferior a 1h, les noves alertes de
        «Mosquits» seguiran comptabilitzant com a part de la mateixa incidència,
        com si no s’hagues publicat cap alerta de «Via Lliure».
    *Si el temps és superior a 1h, comptabilitzará com inici d’una nova incidència.
    """
    llista_controlsMosquits = [] #output

    for avis in llista_tots_avisos:
        if avis["tipus"] == "Mosquitos" or avis["tipus"] == "Mosquits": #Si l'alerta és de Mosquits
            #Recorre tota la llista de controlsMosquits:
                #Per cada control trobat a la mateixa parada on s'ha rebut aquesta alerta, comproba si:
                    #self.controlActiu == True?
                        #Fa menys de 6h de la primera alerta?
                            #self.alertes += 1
                            #continue
                        #Fa més de 6h de la primera alerta?
                            #self.controlActiu = False
                            #self.final = self.inici+6h
                            #Afageix un nou control a la llista de controlsMosquits
                            #break
                    #self.controlActiu == False?
                        #si (hora_actual-self.final < 1h) i (hota_actual-self.inici=<6h):
                            #self.controlActiu = True
                            #self.alertes += 1
                            #continue

            #Afegeix un nou control a la llista de controlsMosquits

        if avis["tipis"] == "Lliure": #Si l'alerta és de Via Lliure
            #Recorre tota la llista de controlsMosquits:
                #Per cada control trobat a la mateixa parada on s'ha rebut aquesta alerta, comproba si:
                    #self.controlActiu == True?
                        #self.controlActiu = False
                        #self.final = hora_actual

    return llista_controlsMosquits
'''
################################################################################
###################### Estudi de les dades obtingudes ##########################

#4.1. Número d'alertes per setmana
def alertes_per_setmana():
    SetmanaIAny = [] #Eix d'abscises
    AvisosPerSetmana = [] #Num. total d'avisos

    MosquitsPerSetmana = [] #Num. d'avisos de Mosquits
    GorillesPerSetmana = [] #Num. d'avisos de Goril·les
    GossosPerSetmana = [] #Num. d'avisos de Gossos
    ViaLliurePerSetmana = [] #Num. d'avisos de Via Lliure
    IncidenciaPerSetmana = [] #Num. d'avisos d'altres incidències
    PreguntesPerSetmana = [] #Num. de preguntes fetes
    AltresAlertesPerSetmana = [] #Cath

    #Troba la quantitat de setmanes que han pasat entre el primer avis i l'ultim
    data_inici = llista_avisos[0]["dia"].split(".")
    data_final = llista_avisos[-1]["dia"].split(".")
    d0 = datetime.date(int(data_inici[2]), int(data_inici[1]), int(data_inici[0]))
    d1 = datetime.date(int(data_final[2]), int(data_final[1]), int(data_final[0]))
    total_setmanes = ceil((d1 - d0).days/7)

    #Confecciona l'eix d'abscices
    for i in range(0, total_setmanes+1):
        setmana_actual = str(d0.isocalendar()[1])+"."+str(d0.isocalendar()[0])
        SetmanaIAny.append(setmana_actual)

        #Inicialitza els eixos d'ordenades
        AvisosPerSetmana.append(0)
        MosquitsPerSetmana.append(0)
        GorillesPerSetmana.append(0)
        GossosPerSetmana.append(0)
        ViaLliurePerSetmana.append(0)
        IncidenciaPerSetmana.append(0)
        PreguntesPerSetmana.append(0)
        AltresAlertesPerSetmana.append(0)

        #Pasa a la següent setmana
        d0 += datetime.timedelta(days=7)

    #Confeciona els eixos d'ordenades
    j = 0
    for avis in llista_avisos: #Per cada avis
        #Troba la setmana de l'avis actual
        data = avis["dia"]
        dia = int(data[0:2])
        mes = int(data[3:5])
        any = int(data[6:10])
        setmana = datetime.date(any, mes, dia).isocalendar()[1]
        if(mes==1 and setmana==52): any -= 1 #Fix pels anys en que l'any no comença en dilluns

        bool_buscant = True
        while(bool_buscant):
            if (str(setmana)+"."+str(any)) == SetmanaIAny[j]:
                AvisosPerSetmana[j] += 1

                if avis["tipus"] == "Lliure":
                    ViaLliurePerSetmana[j] += 1
                elif avis["tipus"] == "Mosquitos" or avis["tipus"] == "Mosquits":
                    MosquitsPerSetmana[j] += 1
                elif avis["tipus"] == "Incidencia":
                    IncidenciaPerSetmana[j] += 1
                elif avis["tipus"] == "Gorilas" or avis["tipus"] == "Goril·les":
                    GorillesPerSetmana[j] += 1
                elif avis["tipus"] == "Gossos":
                    GossosPerSetmana[j] += 1
                elif avis["tipus"] == "Pregunta":
                    PreguntesPerSetmana[j] += 1
                else:
                    AltresAlertesPerSetmana[j] += 1 #Cath

                bool_buscant = False
            j+=1
            if(j>=len(SetmanaIAny)): j=0

    #Regresió lineal entre 47.2021 i 18.2022
    x = [i for i in range(len(SetmanaIAny))]
    x = x[9:-7]
    y = AvisosPerSetmana[9:-7]
    res = stats.linregress(x, y)
    print("Slope: ", res.slope, "Rsquared: ", res.rvalue**2)

    ####Plot de les dades###
    for i in range(len(SetmanaIAny)):
        SetmanaIAny[i] = SetmanaIAny[i][:-5] #Esborra l'any de la data, deix el dia i el mes només

    #Prepara els eixos d'ordenades per a visualitzar-los en barres apilades
    for i in range(0, len(MosquitsPerSetmana)):
        ViaLliurePerSetmana[i] += MosquitsPerSetmana[i]
        GorillesPerSetmana[i] += ViaLliurePerSetmana[i]
        GossosPerSetmana[i] += GorillesPerSetmana[i]
        IncidenciaPerSetmana[i] += GossosPerSetmana[i]
        PreguntesPerSetmana[i] += IncidenciaPerSetmana[i]
        AltresAlertesPerSetmana[i] += PreguntesPerSetmana[i]

    #Dibuixa les dades
    fig, ax = plt.subplots()
    ax.bar(SetmanaIAny, AltresAlertesPerSetmana, label='Altres', color='grey')
    ax.bar(SetmanaIAny, PreguntesPerSetmana, label='Preguntes', color='purple')
    ax.bar(SetmanaIAny, IncidenciaPerSetmana, label='Incidència', color='pink')
    ax.bar(SetmanaIAny, GossosPerSetmana, label='Gossos', color='blue')
    ax.bar(SetmanaIAny, GorillesPerSetmana, label='Goril·les', color='orange')
    ax.bar(SetmanaIAny, ViaLliurePerSetmana, label='Via Lliure', color='green')
    ax.bar(SetmanaIAny, MosquitsPerSetmana, label='Mosquits', color='red')
    ax.set_ylabel("Número d'alertes", fontsize=20)
    ax.set_xlabel("Setmana", fontsize=20)
    ax.set_title("Alertes publicades per setmana", fontsize=24)
    ax.legend(loc='upper left', fontsize=20)
    plt.show()

#4.1 Número d'alertes per dia
def alertes_per_dia():
    Data = [] #Eix d'abscises
    AvisosPerDia = [] #Eix d'ordenades

    MosquitsPerDia = [] #Num. d'avisos de Mosquits
    GorillesPerDia = [] #Num. d'avisos de Goril·les
    GossosPerDia = [] #Num. d'avisos de Gossos
    ViaLliurePerDia = [] #Num. d'avisos de Via Lliure
    IncidenciaPerDia = [] #Num. d'avisos d'altres incidències
    PreguntesPerDia = [] #Num. de preguntes fetes
    AltresAlertesPerDia = [] #Cath

    #Troba la quantitat de dies que han pasat entre el primer avis i l'ultim
    data_inici = llista_avisos[0]["dia"].split(".")
    data_final = llista_avisos[-1]["dia"].split(".")
    d0 = datetime.date(int(data_inici[2]), int(data_inici[1]), int(data_inici[0]))
    d1 = datetime.date(int(data_final[2]), int(data_final[1]), int(data_final[0]))
    total_dies = (d1 - d0).days

    #Completa l'eix d'abscices, afegint cada dia de l'interval estudiat a la llista
    un_dia_qualsevol = d0
    for i in range(0, total_dies+1):
        Data.append(str(f"{un_dia_qualsevol.day:02d}")+"."+str(f"{un_dia_qualsevol.month:02d}")+"."+str(f"{un_dia_qualsevol.year:04d}"))
        un_dia_qualsevol += datetime.timedelta(days=1)

        #Inicialitza els eixos d'ordenades
        AvisosPerDia.append(0)
        MosquitsPerDia.append(0)
        GorillesPerDia.append(0)
        GossosPerDia.append(0)
        ViaLliurePerDia.append(0)
        IncidenciaPerDia.append(0)
        PreguntesPerDia.append(0)
        AltresAlertesPerDia.append(0)

    #Completa l'eix d'ordenades
    j = 0
    for avis in llista_avisos:
        bool_buscant = True
        while(bool_buscant):
            if avis["dia"] == Data[j]:
                AvisosPerDia[j] += 1

                if avis["tipus"] == "Lliure":
                    ViaLliurePerDia[j] += 1
                elif avis["tipus"] == "Mosquitos" or avis["tipus"] == "Mosquits":
                    MosquitsPerDia[j] += 1
                elif avis["tipus"] == "Incidencia":
                    IncidenciaPerDia[j] += 1
                elif avis["tipus"] == "Gorilas" or avis["tipus"] == "Goril·les":
                    GorillesPerDia[j] += 1
                elif avis["tipus"] == "Gossos":
                    GossosPerDia[j] += 1
                elif avis["tipus"] == "Pregunta":
                    PreguntesPerDia[j] += 1
                else:
                    AltresAlertesPerDia[j] += 1 #Cath

                bool_buscant = False
            j+=1
            if(j>=len(Data)): j=0

    ####Dibuixa el grafic###
    for i in range(len(Data)):
        Data[i] = Data[i][:-5] #Esborra l'any de la data, deix el dia i el mes només

    #Prepara els eixos d'ordenades per a visualitzar-los en barres apilades
    for i in range(0, len(MosquitsPerDia)):
        ViaLliurePerDia[i] += MosquitsPerDia[i]
        GorillesPerDia[i] += ViaLliurePerDia[i]
        GossosPerDia[i] += GorillesPerDia[i]
        IncidenciaPerDia[i] += GossosPerDia[i]
        PreguntesPerDia[i] += IncidenciaPerDia[i]
        AltresAlertesPerDia[i] += PreguntesPerDia[i]

    #Dibuixa les dades
    fig, ax = plt.subplots()
    ax.bar(Data, AltresAlertesPerDia, label='Altres', color='grey')
    ax.bar(Data, PreguntesPerDia, label='Preguntes', color='purple')
    ax.bar(Data, IncidenciaPerDia, label='Incidència', color='pink')
    ax.bar(Data, GossosPerDia, label='Gossos', color='blue')
    ax.bar(Data, GorillesPerDia, label='Goril·les', color='orange')
    ax.bar(Data, ViaLliurePerDia, label='Via Lliure', color='green')
    ax.bar(Data, MosquitsPerDia, label='Mosquits', color='red')
    ax.set_xticks([x*15 for x in range(int(len(AvisosPerDia)/15)+1)])
    ax.set_ylabel("Número d'alertes", fontsize=20)
    ax.set_xlabel("Dia", fontsize=20)
    ax.set_title("Alertes publicades per dia", fontsize=24)
    ax.legend(loc='upper left', fontsize=20)
    plt.show()

#4.2 Número d’alertes publicades per dia de la setmana
def alertes_per_dia_de_la_setmana():
    dies = ["Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge"]
    alertes_Mosquits = [0]*7
    alertes_Goriles = [0]*7
    alertes_Gossos = [0]*7
    alertes_ViaLliure = [0]*7
    alertes_Incidencia = [0]*7
    alertes_Preguntes = [0]*7
    alertes_Altres = [0]*7

    #Compta les alertes/dia de la setmana que s'han produit, separades per tipus d'alerta
    for avis in llista_avisos:
        data = avis["dia"]
        dia = int(data[0:2])
        mes = int(data[3:5])
        any = int(data[6:10])
        dia_de_la_setmana =  datetime.date(any, mes, dia).weekday()

        if(dia_de_la_setmana == 6):
            print("Diumenge: ", avis["dia"], avis["hora"])
        elif(dia_de_la_setmana == 5):
            print("Dissabte: ", avis["dia"], avis["hora"])

        if avis["tipus"] == "Lliure":
            alertes_ViaLliure[dia_de_la_setmana] += 1
        elif avis["tipus"] == "Mosquitos" or avis["tipus"] == "Mosquits":
            alertes_Mosquits[dia_de_la_setmana] += 1
        elif avis["tipus"] == "Incidencia":
            alertes_Incidencia[dia_de_la_setmana] += 1
        elif avis["tipus"] == "Gorilas" or avis["tipus"] == "Goril·les":
            alertes_Goriles[dia_de_la_setmana] += 1
        elif avis["tipus"] == "Gossos":
            alertes_Gossos[dia_de_la_setmana] += 1
        elif avis["tipus"] == "Pregunta":
            alertes_Preguntes[dia_de_la_setmana] += 1
        else:
            alertes_Altres[dia_de_la_setmana] += 1 #Cath

    #Prepara els eixos d'ordenades per a visualitzar-los en barres apilades
    for i in range(0, len(alertes_Mosquits)):
        alertes_ViaLliure[i] += alertes_Mosquits[i]
        alertes_Goriles[i] += alertes_ViaLliure[i]
        alertes_Gossos[i] += alertes_Goriles[i]
        alertes_Incidencia[i] += alertes_Gossos[i]
        alertes_Preguntes[i] += alertes_Incidencia[i]
        alertes_Altres[i] += alertes_Preguntes[i]

    #Dibuixa les dades
    fig, ax = plt.subplots()
    ax.bar(dies, alertes_Altres, label='Altres', color='grey')
    ax.bar(dies, alertes_Preguntes, label='Preguntes', color='purple')
    ax.bar(dies, alertes_Incidencia, label='Incidència', color='pink')
    ax.bar(dies, alertes_Gossos, label='Gossos', color='blue')
    ax.bar(dies, alertes_Goriles, label='Goril·les', color='orange')
    ax.bar(dies, alertes_ViaLliure, label='Via Lliure', color='green')
    ax.bar(dies, alertes_Mosquits, label='Mosquits', color='red')
    #ax.set_xticks([x*15 for x in range(int(len(AvisosPerDia)/15)+1)])
    ax.set_ylabel("Número d'alelrtes", fontsize=20)
    ax.set_xlabel("Dia", fontsize=20)
    ax.set_title("Alertes publicades per dia de la setmana", fontsize=24)
    ax.legend(loc='upper right', fontsize=15)
    plt.show()

#4.3 Hores en que es produeixen les alertes
def alertes_per_hora():
    #Crea l'eix x:
    franja_horaria = []
    hora = 0
    minut = 0
    semafor = True
    for i in range(48): #48 Franjes de 30min
        franja_horaria.append("{:02d}".format(hora)+":{:02d}".format(minut))
        minut += 30
        semafor = not(semafor)
        if semafor:
            minut = 0
            hora += 1

    #Inicialitza els eixos y
    alertes_ViaLliure = 48*[0]
    alertes_Mosquits = 48*[0]
    alertes_Incidencia = 48*[0]
    alertes_Goriles = 48*[0]
    alertes_Gossos = 48*[0]
    alertes_Preguntes = 48*[0]
    alertes_Altres = 48*[0]

    #Completa els eixos y:
    for avis in llista_avisos:
        #Troba la franja horaria en que s'ha produit l'avis
        franja_hora_avis = 2*int(avis["hora"][0:2])+1*(int(avis["hora"][3:5])>=30)

        #Registra la informació a la llista corresponent:
        if avis["tipus"] == "Lliure":
            alertes_ViaLliure[franja_hora_avis] += 1
        elif avis["tipus"] == "Mosquitos" or avis["tipus"] == "Mosquits":
            alertes_Mosquits[franja_hora_avis] += 1
        elif avis["tipus"] == "Incidencia":
            alertes_Incidencia[franja_hora_avis] += 1
        elif avis["tipus"] == "Gorilas" or avis["tipus"] == "Goril·les":
            alertes_Goriles[franja_hora_avis] += 1
        elif avis["tipus"] == "Gossos":
            alertes_Gossos[franja_hora_avis] += 1
        elif avis["tipus"] == "Pregunta":
            alertes_Preguntes[franja_hora_avis] += 1
        else:
            alertes_Altres[franja_hora_avis] += 1 #Cath

    #Prepara els eixos d'ordenades per a visualitzar-los en barres apilades
    for i in range(0, len(alertes_Mosquits)):
        alertes_ViaLliure[i] += alertes_Mosquits[i]
        alertes_Goriles[i] += alertes_ViaLliure[i]
        alertes_Gossos[i] += alertes_Goriles[i]
        alertes_Incidencia[i] += alertes_Gossos[i]
        alertes_Preguntes[i] += alertes_Incidencia[i]
        alertes_Altres[i] += alertes_Preguntes[i]

    #Dibuixa les dades
    fig, ax = plt.subplots()
    ax.bar(franja_horaria, alertes_Altres, label='Altres', color='grey')
    ax.bar(franja_horaria, alertes_Preguntes, label='Preguntes', color='purple')
    ax.bar(franja_horaria, alertes_Incidencia, label='Incidència', color='pink')
    ax.bar(franja_horaria, alertes_Gossos, label='Gossos', color='blue')
    ax.bar(franja_horaria, alertes_Goriles, label='Goril·les', color='orange')
    ax.bar(franja_horaria, alertes_ViaLliure, label='Via Lliure', color='green')
    ax.bar(franja_horaria, alertes_Mosquits, label='Mosquits', color='red')
    #ax.set_xticks([x*15 for x in range(int(len(AvisosPerDia)/15)+1)])
    ax.set_ylabel("Número d'alertes", fontsize=20)
    ax.set_xlabel("Hora", fontsize=20)
    ax.tick_params(axis='x', rotation=70)
    ax.set_title("Alertes publicades per franja horària", fontsize=24)
    ax.legend(loc='upper right', fontsize=15)
    plt.show()

def parades_amb_mosquits():
    """Compta el número d'alertes de mosquits que ha rebut cada parada
    també el número d'alertes de mosquits que ha rebut cada línea"""

    lineas_amb_mosquits = [] #Array de strings amb el nom de les líneas afectades (eix X)
    mosquits_per_linea = [] #recompte de mosquits que han hagut en cada línea (eix Y)

    parades_amb_mosquits = [] #Array amb els noms de les parades amb Mosquits
    mosquits_per_parada = [] #recompte de mosquits que han hagut en cada parada (eix Y)


    for avis in llista_avisos:
        if avis["tipus"] == "Mosquitos" or avis["tipus"] == "Mosquits":

            #Crea l'eix X del primer gràfic
            if not(avis["linea"] in lineas_amb_mosquits):
                lineas_amb_mosquits.append(avis["linea"])
                mosquits_per_linea.append(0)

            #Crea l'eix X del segón gràfic
            if not(avis["parada"] in parades_amb_mosquits):
                parades_amb_mosquits.append(avis["parada"])
                mosquits_per_parada.append(0)

            #Crea l'eix Y del primer gràfic
            for i in range(len(lineas_amb_mosquits)):
                if lineas_amb_mosquits[i] == avis["linea"]:
                    mosquits_per_linea[i] += 1

            #Crea l'eix Y del segón gràfic
            for i in range(len(parades_amb_mosquits)):
                if parades_amb_mosquits[i] == avis["parada"]:
                    mosquits_per_parada[i] += 1

    print(lineas_amb_mosquits)

    #Dibuixa els gràfics
    fig, ax = plt.subplots()
    ax.bar(lineas_amb_mosquits, mosquits_per_linea)
    ax.set_ylabel("Quantitat", fontsize=20)
    ax.set_xlabel("Línea de transport", fontsize=20)
    ax.set_title("Alertes de Mosquits per línea", fontsize=24)
    ax.tick_params(axis='x', rotation=70)
    plt.show()

    print(parades_amb_mosquits)


    fig, ax = plt.subplots()
    ax.bar(parades_amb_mosquits, mosquits_per_parada)
    ax.set_ylabel("Quantitat", fontsize=20)
    ax.set_xlabel("Parada", fontsize=20)
    ax.set_title("Alertes de Mosquits per parada", fontsize=24)
    ax.tick_params(axis='x', rotation=70)
    plt.show()

################################################################################
########################## Executa #############################################
alertes_per_setmana()
alertes_per_dia()
alertes_per_dia_de_la_setmana()
alertes_per_hora()
parades_amb_mosquits()
################################################################################
