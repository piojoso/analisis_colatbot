import telegram
from telegram import InputMediaPhoto
import settings
import os, os.path
import tweepy


class Publisher(object):
    images = []

    def __init__(self, images_folder = ""):
        if images_folder != "":
            self.get_images_from_folder(images_folder)

    def get_images_from_folder(self, images_folder):
        path = images_folder
        valid_images = [".jpg", ".gif", ".png"]
        for f in os.listdir(path):
            ext = os.path.splitext(f)[1]
            if ext.lower() not in valid_images:
                continue
            self.images.append(os.path.join(path, f))

    ##### Telegram
    def publish_telegram(self, text: str):
        bot = telegram.Bot(token=settings.TELETOKEN)
        media_group = []
        for i, img in enumerate(self.images):
            media_group.append(InputMediaPhoto(open(img, 'rb'), caption=text if i == 0 else ''))
        print("Sending telegram")
        for chat in settings.TELECHATID:
            bot.send_media_group(chat_id=chat, media=media_group)

    ##### Tweeter
    def publish_tweeter(self, msg: str):
        auth = tweepy.OAuthHandler(settings.TWITTER_CONSUMER_KEY, settings.TWITTER_CONSUMER_SECRET)
        auth.set_access_token(settings.TWITTER_ACCESS_TOKEN, settings.TWITTER_ACCESS_TOKEN_SECRET)
        api = tweepy.API(auth)

        def send_tweet(imgs):
            media_ids = []
            for filename in imgs:
                print(filename)
                res = api.media_upload(filename)
                media_ids.append(res.media_id)
            print("Sending twitter")
            # Tweet with multiple images
            api.update_status(status=msg, media_ids=media_ids)

        if self.images.__len__() < 4:
            send_tweet(self.images)
        else:
            k = 4
            split = [self.images[i:i + k] for i in range(0, len(self.images), k)]
            for s in split:
                send_tweet(s)

    def publish(self, tlg_msg:str, twt_msg:str):
        if self.images.__len__() > 0:
            print("Images found")
            print(self.images)
            if hasattr(settings, "TELETOKEN"):
                self.publish_telegram(tlg_msg)
            if hasattr(settings, "TWITTER_CONSUMER_KEY") and hasattr(settings, "TWITTER_CONSUMER_SECRET") and hasattr(
                    settings, "TWITTER_ACCESS_TOKEN") and hasattr(settings, "TWITTER_ACCESS_TOKEN_SECRET"):
                self.publish_tweeter(twt_msg)
        else:
            print("Error: no Images found!")

    def publish_text(self, tlg_msg:str, twt_msg:str):
        teleBot = telegram.Bot(token=settings.TELETOKEN)

        if hasattr(settings, "TELETOKEN"):
            for chat in settings.TELECHATID:
                teleBot.send_message(chat_id=chat, text=tlg_msg)

        if hasattr(settings, "TWITTER_CONSUMER_KEY") and hasattr(settings, "TWITTER_CONSUMER_SECRET") and hasattr(
                    settings, "TWITTER_ACCESS_TOKEN") and hasattr(settings, "TWITTER_ACCESS_TOKEN_SECRET"):
            auth = tweepy.OAuthHandler(settings.TWITTER_CONSUMER_KEY, settings.TWITTER_CONSUMER_SECRET)
            auth.set_access_token(settings.TWITTER_ACCESS_TOKEN, settings.TWITTER_ACCESS_TOKEN_SECRET)
            api = tweepy.API(auth)

            api.update_status(status=twt_msg,)

