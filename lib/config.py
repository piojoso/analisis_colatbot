from enum import Enum

class Alerts(Enum):
    Preguntes = ("Preguntes", "#8bd3c7")  # Turquoise
    Incidencia = ("Incidència", "#ffee65")  # Yellow
    Gossos = ("Gossos", "#7eb0d5")  # Blue
    Goriles = ("Goril·les", "#ffb55a")  # Orange
    Lliure = ("Via Lliure", "#b2e061")  # Green
    Mosquits = ("Mosquits", "#fd7f6f")  # Red

    def __init__(self, label, color):
        self.label = label
        self.color = color


DEFAULT_COLOR = Alerts.Gossos.color
