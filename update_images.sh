#!/bin/bash

#Where the images should be saved
OUT_DIR=images

#Amount of days to analyze
NUM_DAYS=15

#Create the output directory if it doesn't exist yet
if [ ! -d "$OUT_DIR" ]; then
  mkdir $OUT_DIR
fi


#Create the images with the new data
python3 create_images.py $NUM_DAYS $OUT_DIR && python3 publish.py $OUT_DIR
