import settings
import sys

import settings
from publish import Publisher

txt = """⚠️La App de Memetro no funciona! ⚠️ #MemetroBCN
Per problemes tècnics la app de Memetro esta caiguda des de ahir a la tarda!
Esperem restablir el servei aviat!
Podeu seguir veient alertes a Telegram https://t.me/MemetroBCN i llençarles des del bot https://t.me/ColatBarcelonaBot"""

publisher = Publisher()

publisher.publish_text(tlg_msg=txt, twt_msg=txt)