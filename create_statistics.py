# Creates few images with the data publushed on https://memetro.org:8082/alerts/?daysAgo=15
# Newer version of the colatbot_estadistica.py script
import itertools
import os
from collections import Counter
from PIL import Image, ImageDraw, ImageFont

import matplotlib.pyplot as plt
import json, datetime, urllib.request, sys
from datetime import datetime, timedelta
from scipy import stats
from math import ceil

from lib.config import Alerts, DEFAULT_COLOR

if len(sys.argv) != 3: sys.exit("Usage: python3 create_statistics.py NUM_DAYS_TO_ANALYZE OUT_DIR")

NUM_DAYS_TO_ANALYZE = sys.argv[1]
OUTPUT_FOLDER = sys.argv[2]
URL_DATA = "https://memetro.org:8082/alerts/?daysAgo="

SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))

if not os.path.exists(OUTPUT_FOLDER):
    os.makedirs(OUTPUT_FOLDER)

# Load the alerts from the past N days in memory
with urllib.request.urlopen(URL_DATA + NUM_DAYS_TO_ANALYZE) as url:
    list_alerts = json.load(url)

# Load the decodification info in memory
with urllib.request.urlopen("https://memetro.org:8082/synchronize/tokenfree/") as url:
    synchronize_info = json.load(url)

# Decode the alert type, station_id, transport_id, etc... (to human readible)
for alert in list_alerts:
    alert['line_id'] = synchronize_info['data']['line']['data'][alert['line_id'] - 1]['name']
    alert['city_id'] = synchronize_info['data']['city']['data'][alert['city_id'] - 1]['name']
    alert['station_id'] = synchronize_info['data']['station']['data'][alert['station_id'] - 1]['name']
    alert['transport_id'] = synchronize_info['data']['transport']['data'][alert['transport_id'] - 1]['name']
    alert['alert_type'] = synchronize_info['data']['alert_types']['data'][alert['alert_type'] - 1]['name']
    alert['date'] = datetime.fromisoformat(alert['date'])


# 4.1. Número d'alertes per setmana
def alertes_per_setmana():
    SetmanaIAny = []  # Eix d'abscises
    AvisosPerSetmana = []  # Num. total d'avisos

    MosquitsPerSetmana = []  # Num. d'avisos de Mosquits
    GorillesPerSetmana = []  # Num. d'avisos de Goril·les
    GossosPerSetmana = []  # Num. d'avisos de Gossos
    ViaLliurePerSetmana = []  # Num. d'avisos de Via Lliure
    IncidenciaPerSetmana = []  # Num. d'avisos d'altres incidències
    PreguntesPerSetmana = []  # Num. de preguntes fetes
    AltresAlertesPerSetmana = []  # Cath

    # Troba la quantitat de setmanes que han pasat entre el primer avis i l'ultim
    total_setmanes = ceil((list_alerts[-1]['date'] - list_alerts[0]['date']).days / 7)
    setmana_actual = list_alerts[0]['date']

    # Confecciona l'eix d'abscices
    for i in range(total_setmanes + 1):
        SetmanaIAny.append(str(setmana_actual.isocalendar()[1]) + "." + str(setmana_actual.isocalendar()[0]))

        # Inicialitza els eixos d'ordenades
        AvisosPerSetmana.append(0)
        MosquitsPerSetmana.append(0)
        GorillesPerSetmana.append(0)
        GossosPerSetmana.append(0)
        ViaLliurePerSetmana.append(0)
        IncidenciaPerSetmana.append(0)
        PreguntesPerSetmana.append(0)

        # Pasa a la següent setmana
        setmana_actual += timedelta(days=7)

    # Confeciona els eixos d'ordenades
    j = 0
    for alert in list_alerts:  # Per cada avis
        # Troba la setmana de l'avis actual
        setmana = alert['date'].isocalendar()[1]
        any = alert['date'].isocalendar()[0]
        bool_buscant = True
        while (bool_buscant):
            if (str(setmana) + "." + str(any)) == SetmanaIAny[j]:
                AvisosPerSetmana[j] += 1
                if alert["alert_type"] == synchronize_info['data']['alert_types']['data'][0]['name']:
                    MosquitsPerSetmana[j] += 1
                elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][1]['name']:
                    GorillesPerSetmana[j] += 1
                elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][2]['name']:
                    GossosPerSetmana[j] += 1
                elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][3]['name']:
                    ViaLliurePerSetmana[j] += 1
                elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][4]['name']:
                    IncidenciaPerSetmana[j] += 1
                elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][5]['name']:
                    PreguntesPerSetmana[j] += 1

                bool_buscant = False
            j += 1
            if (j >= len(SetmanaIAny)): j = 0

    ####Plot de les dades###
    for i in range(len(SetmanaIAny)):
        SetmanaIAny[i] = SetmanaIAny[i][:-5]  # Esborra l'any de la data, deixa la setmana i el mes només

    # Prepara els eixos d'ordenades per a visualitzar-los en barres apilades
    for i in range(0, len(MosquitsPerSetmana)):
        ViaLliurePerSetmana[i] += MosquitsPerSetmana[i]
        GorillesPerSetmana[i] += ViaLliurePerSetmana[i]
        GossosPerSetmana[i] += GorillesPerSetmana[i]
        IncidenciaPerSetmana[i] += GossosPerSetmana[i]
        PreguntesPerSetmana[i] += IncidenciaPerSetmana[i]

    # Dibuixa les dades
    fig, ax = plt.subplots(figsize=(8, 8))
    ax.bar(SetmanaIAny, PreguntesPerSetmana, label=Alerts.Preguntes.label, color=Alerts.Preguntes.color)
    ax.bar(SetmanaIAny, IncidenciaPerSetmana, label=Alerts.Incidencia.label, color=Alerts.Incidencia.color)
    ax.bar(SetmanaIAny, GossosPerSetmana, label=Alerts.Gossos.label, color=Alerts.Gossos.color)
    ax.bar(SetmanaIAny, GorillesPerSetmana, label=Alerts.Goriles.label, color=Alerts.Goriles.color)
    ax.bar(SetmanaIAny, ViaLliurePerSetmana, label=Alerts.Lliure.label, color=Alerts.Lliure.color)
    ax.bar(SetmanaIAny, MosquitsPerSetmana, label=Alerts.Mosquits.label, color=Alerts.Mosquits.color)
    ax.set_ylabel("Número d'alertes", fontsize=20)
    ax.set_xlabel("Setmana", fontsize=20)
    ax.set_title("Alertes publicades per setmana", fontsize=24)
    ax.legend(loc='upper right', fontsize=14)
    plt.savefig(OUTPUT_FOLDER + '/alerts_per_week.png')


# Create the images
def alertes_per_dia():
    Data = []  # Eix d'abscises
    AvisosPerDia = []  # Eix d'ordenades

    MosquitsPerDia = []  # Num. d'avisos de Mosquits
    GorillesPerDia = []  # Num. d'avisos de Goril·les
    GossosPerDia = []  # Num. d'avisos de Gossos
    ViaLliurePerDia = []  # Num. d'avisos de Via Lliure
    IncidenciaPerDia = []  # Num. d'avisos d'altres incidències
    PreguntesPerDia = []  # Num. de preguntes fetes

    # Completa l'eix d'abscices, afegint cada dia de l'interval estudiat a la llista
    first_day = list_alerts[0]['date'].date()
    last_day = list_alerts[-1]['date'].date()
    while ((last_day - first_day).days >= 0):
        Data.append(str(f"{first_day.day:02d}") + "." + str(f"{first_day.month:02d}"))
        first_day += timedelta(days=1)

        # Inicialitza els eixos d'ordenades
        AvisosPerDia.append(0)
        MosquitsPerDia.append(0)
        GorillesPerDia.append(0)
        GossosPerDia.append(0)
        ViaLliurePerDia.append(0)
        IncidenciaPerDia.append(0)
        PreguntesPerDia.append(0)

    # Completa l'eix d'ordenades
    j = 0
    for alert in list_alerts:
        bool_buscant = True
        current_day = alert['date'].strftime('%d.%m')
        while (bool_buscant):
            if current_day == Data[j]:
                AvisosPerDia[j] += 1
                if alert["alert_type"] == synchronize_info['data']['alert_types']['data'][0]['name']:
                    MosquitsPerDia[j] += 1
                elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][1]['name']:
                    GorillesPerDia[j] += 1
                elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][2]['name']:
                    GossosPerDia[j] += 1
                elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][3]['name']:
                    ViaLliurePerDia[j] += 1
                elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][4]['name']:
                    IncidenciaPerDia[j] += 1
                elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][5]['name']:
                    PreguntesPerDia[j] += 1

                bool_buscant = False
            j += 1
            if (j >= len(Data)): j = 0

    ####Dibuixa el grafic###
    # Prepara els eixos d'ordenades per a visualitzar-los en barres apilades
    for i in range(0, len(MosquitsPerDia)):
        ViaLliurePerDia[i] += MosquitsPerDia[i]
        GorillesPerDia[i] += ViaLliurePerDia[i]
        GossosPerDia[i] += GorillesPerDia[i]
        IncidenciaPerDia[i] += GossosPerDia[i]
        PreguntesPerDia[i] += IncidenciaPerDia[i]

    # Dibuixa les dades
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.bar(Data, PreguntesPerDia, label=Alerts.Preguntes.label, color=Alerts.Preguntes.color)
    ax.bar(Data, IncidenciaPerDia, label=Alerts.Incidencia.label, color=Alerts.Incidencia.color)
    ax.bar(Data, GossosPerDia, label=Alerts.Gossos.label, color=Alerts.Gossos.color)
    ax.bar(Data, GorillesPerDia, label=Alerts.Goriles.label, color=Alerts.Goriles.color)
    ax.bar(Data, ViaLliurePerDia, label=Alerts.Lliure.label, color=Alerts.Lliure.color)
    ax.bar(Data, MosquitsPerDia, label=Alerts.Mosquits.label, color=Alerts.Mosquits.color)
    ax.set_ylabel("Número d'alertes", fontsize=20)
    ax.set_xlabel("Dia", fontsize=20)
    ax.set_title("Alertes dels últims " + NUM_DAYS_TO_ANALYZE + " dies", fontsize=24)
    ax.legend(loc='upper right', fontsize=14)
    plt.savefig(OUTPUT_FOLDER + '/alerts_per_day.png')


# 4.2 Número d’alertes publicades per dia de la setmana
def alertes_per_dia_de_la_setmana():
    dies = ["Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge"]
    alertes_Mosquits = [0] * 7
    alertes_Goriles = [0] * 7
    alertes_Gossos = [0] * 7
    alertes_ViaLliure = [0] * 7
    alertes_Incidencia = [0] * 7
    alertes_Preguntes = [0] * 7

    # Compta les alertes/dia de la setmana que s'han produit, separades per tipus d'alerta
    for alert in list_alerts:
        # data = alert['date'].weekday
        # dia = data.day
        # mes = data.month
        # any = data.year
        dia_de_la_setmana = alert['date'].weekday()

        if alert["alert_type"] == synchronize_info['data']['alert_types']['data'][0]['name']:
            alertes_Mosquits[dia_de_la_setmana] += 1
        elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][1]['name']:
            alertes_Goriles[dia_de_la_setmana] += 1
        elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][2]['name']:
            alertes_Gossos[dia_de_la_setmana] += 1
        elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][3]['name']:
            alertes_ViaLliure[dia_de_la_setmana] += 1
        elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][4]['name']:
            alertes_Incidencia[dia_de_la_setmana] += 1
        elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][5]['name']:
            alertes_Preguntes[dia_de_la_setmana] += 1

    # Before prepare to use it on the chart, store the totals for the pie chart
    sizes = [
        sum(alertes_Preguntes),
        sum(alertes_Incidencia),
        sum(alertes_Gossos),
        sum(alertes_Goriles),
        sum(alertes_ViaLliure),
        sum(alertes_Mosquits),
    ]

    # Prepara els eixos d'ordenades per a visualitzar-los en barres apilades
    for i in range(0, len(alertes_Mosquits)):
        alertes_ViaLliure[i] += alertes_Mosquits[i]
        alertes_Goriles[i] += alertes_ViaLliure[i]
        alertes_Gossos[i] += alertes_Goriles[i]
        alertes_Incidencia[i] += alertes_Gossos[i]
        alertes_Preguntes[i] += alertes_Incidencia[i]

    # Dibuixa les dades
    fig, ax = plt.subplots(figsize=(8, 8))
    ax.bar(dies, alertes_Preguntes, label=Alerts.Preguntes.label, color=Alerts.Preguntes.color)
    ax.bar(dies, alertes_Incidencia, label=Alerts.Incidencia.label, color=Alerts.Incidencia.color)
    ax.bar(dies, alertes_Gossos, label=Alerts.Gossos.label, color=Alerts.Gossos.color)
    ax.bar(dies, alertes_Goriles, label=Alerts.Goriles.label, color=Alerts.Goriles.color)
    ax.bar(dies, alertes_ViaLliure, label=Alerts.Lliure.label, color=Alerts.Lliure.color)
    ax.bar(dies, alertes_Mosquits, label=Alerts.Mosquits.label, color=Alerts.Mosquits.color)
    # ax.set_xticks([x*15 for x in range(int(len(AvisosPerDia)/15)+1)])
    ax.set_ylabel("Número d'alelrtes", fontsize=20)
    ax.set_xlabel("Dia", fontsize=20)
    ax.set_title("Alertes publicades per dia de la setmana", fontsize=24)
    ax.legend(loc='upper right', fontsize=14)
    plt.savefig(OUTPUT_FOLDER + '/alerts_per_weekday.png')

    # Totals Pie
    labels = [
        Alerts.Preguntes.label,
        Alerts.Incidencia.label,
        Alerts.Gossos.label,
        Alerts.Goriles.label,
        Alerts.Lliure.label,
        Alerts.Mosquits.label,
    ]
    colors = [
        Alerts.Preguntes.color,
        Alerts.Incidencia.color,
        Alerts.Gossos.color,
        Alerts.Goriles.color,
        Alerts.Lliure.color,
        Alerts.Mosquits.color,
    ]

    # To dont use percentage on the pie
    def totals_count(pct, allvals):
        absolute = int(pct/100.*sum(allvals))
        return f"{absolute:d}"

    # Removing entries with a total count of 0
    sizes, labels, colors = zip(*[(size, label, colors) for size, label, colors in zip(sizes, labels, colors) if size > 0])

    # Plotting the pie chart
    fig, ax = plt.subplots()
    ax.pie(sizes, labels=labels, colors=colors,  autopct=lambda pct: totals_count(pct, sizes),  startangle=140)
    ax.axis('equal')  # Equal aspect ratio ensures the pie chart is circular.
    ax.set_title("Total tipus d'alertes", fontsize=24)
    plt.savefig(OUTPUT_FOLDER + '/total_alert_types.png')



# 4.3 Hores en que es produeixen les alertes
def alertes_per_hora():
    # Crea l'eix x:
    franja_horaria = []
    hora = 0
    minut = 0
    semafor = True
    for i in range(48):  # 48 Franjes de 30min
        franja_horaria.append("{:02d}".format(hora) + ":{:02d}".format(minut))
        minut += 30
        semafor = not (semafor)
        if semafor:
            minut = 0
            hora += 1

    # Inicialitza els eixos y
    alertes_ViaLliure = 48 * [0]
    alertes_Mosquits = 48 * [0]
    alertes_Incidencia = 48 * [0]
    alertes_Goriles = 48 * [0]
    alertes_Gossos = 48 * [0]
    alertes_Preguntes = 48 * [0]

    # Completa els eixos y:
    for alert in list_alerts:
        # Troba la franja horaria en que s'ha produit l'avis
        franja_hora_avis = 2 * (alert['date'].hour) + 1 * ((alert['date'].minute) >= 30)

        # Registra la informació a la llista corresponent:
        if alert["alert_type"] == synchronize_info['data']['alert_types']['data'][0]['name']:
            alertes_Mosquits[franja_hora_avis] += 1
        elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][1]['name']:
            alertes_Goriles[franja_hora_avis] += 1
        elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][2]['name']:
            alertes_Gossos[franja_hora_avis] += 1
        elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][3]['name']:
            alertes_ViaLliure[franja_hora_avis] += 1
        elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][4]['name']:
            alertes_Incidencia[franja_hora_avis] += 1
        elif alert["alert_type"] == synchronize_info['data']['alert_types']['data'][5]['name']:
            alertes_Preguntes[franja_hora_avis] += 1

    # Prepara els eixos d'ordenades per a visualitzar-los en barres apilades
    for i in range(0, len(alertes_Mosquits)):
        alertes_ViaLliure[i] += alertes_Mosquits[i]
        alertes_Goriles[i] += alertes_ViaLliure[i]
        alertes_Gossos[i] += alertes_Goriles[i]
        alertes_Incidencia[i] += alertes_Gossos[i]
        alertes_Preguntes[i] += alertes_Incidencia[i]

    # Dibuixa les dades
    fig, ax = plt.subplots(figsize=(12, 12))
    ax.bar(franja_horaria, alertes_Preguntes, label=Alerts.Preguntes.label, color=Alerts.Preguntes.color)
    ax.bar(franja_horaria, alertes_Incidencia, label=Alerts.Incidencia.label, color=Alerts.Incidencia.color)
    ax.bar(franja_horaria, alertes_Gossos, label=Alerts.Gossos.label, color=Alerts.Gossos.color)
    ax.bar(franja_horaria, alertes_Goriles, label=Alerts.Goriles.label, color=Alerts.Goriles.color)
    ax.bar(franja_horaria, alertes_ViaLliure, label=Alerts.Lliure.label, color=Alerts.Lliure.color)
    ax.bar(franja_horaria, alertes_Mosquits, label=Alerts.Mosquits.label, color=Alerts.Mosquits.color)
    ax.set_ylabel("Número d'alertes", fontsize=20)
    ax.set_xlabel("Hora", fontsize=20)
    ax.tick_params(axis='x', rotation=70)
    ax.set_title("Alertes publicades per franja horària", fontsize=24)
    ax.legend(loc='upper right', fontsize=15)
    plt.savefig(OUTPUT_FOLDER + '/alerts_per_hour.png')


# Parades a les que s'han rebut les alertes
def parades_amb_mosquits():
    # Compta el número d'alertes de mosquits que ha rebut cada parada
    # també el número d'alertes de mosquits que ha rebut cada línea

    lineas_amb_mosquits = []  # Array de strings amb el nom de les líneas afectades (eix X)
    mosquits_per_linea = []  # recompte de mosquits que han hagut en cada línea (eix Y)

    parades_amb_mosquits = []  # Array amb els noms de les parades amb Mosquits
    mosquits_per_parada = []  # recompte de mosquits que han hagut en cada parada (eix Y)

    for alert in list_alerts:
        if alert["alert_type"] == synchronize_info['data']['alert_types']['data'][0]['name']:

            # Crea l'eix X del primer gràfic
            if not (alert["line_id"] in lineas_amb_mosquits):
                lineas_amb_mosquits.append(alert["line_id"])
                mosquits_per_linea.append(0)

            # Crea l'eix X del segón gràfic
            if not (alert["station_id"] in parades_amb_mosquits):
                if alert[
                    "station_id"] == "no_station":  # no_station happens in bus alerts. Change their name for one more suited
                    alert["station_id"] = "Bus totes"
                if alert["station_id"] == "Magòria La Campana":  # Name too long for the graphic
                    alert["station_id"] = "La Campana"
                if alert["station_id"] == "Rambla Just Oliveras":  # Name too long for the graphic
                    alert["station_id"] = "Just Oliveras"

                parades_amb_mosquits.append(alert["station_id"])
                mosquits_per_parada.append(0)

            # Crea l'eix Y del primer gràfic
            for i in range(len(lineas_amb_mosquits)):
                if lineas_amb_mosquits[i] == alert["line_id"]:
                    mosquits_per_linea[i] += 1

            # Crea l'eix Y del segón gràfic
            for i in range(len(parades_amb_mosquits)):
                if parades_amb_mosquits[i] == alert["station_id"]:
                    mosquits_per_parada[i] += 1

            # Give the alerts their original name again, so the general variable reminds unchanged after this function
            if alert["station_id"] == "Bus totes":
                alert["station_id"] = "no_station"
            if alert["station_id"] == "La Campana":
                alert["station_id"] = "Magòria La Campana"
            if alert["station_id"] == "Just Oliveras":
                alert["station_id"] = "Rambla Just Oliveras"

    # Sort the results:
    sorted_lines = sorted(zip(mosquits_per_linea, lineas_amb_mosquits), reverse=True)
    sorted_stations = sorted(zip(mosquits_per_parada, parades_amb_mosquits), reverse=True)

    lineas_amb_mosquits = [x[1] for x in sorted_lines]
    mosquits_per_linea = [x[0] for x in sorted_lines]
    parades_amb_mosquits = [x[1] for x in sorted_stations]
    mosquits_per_parada = [x[0] for x in sorted_stations]

    # Dibuixa els gràfics
    fig, ax = plt.subplots(figsize=(16, 16))
    ax.bar(lineas_amb_mosquits, mosquits_per_linea, color=DEFAULT_COLOR)
    ax.set_ylabel("Quantitat", fontsize=20)
    ax.set_xlabel("Línea de transport", fontsize=20)
    ax.set_title("Alertes de Mosquits per línea ultims " + NUM_DAYS_TO_ANALYZE + " dies", fontsize=24)
    plt.xticks(fontsize=14, rotation=60, ha='right')
    plt.savefig(OUTPUT_FOLDER + '/mosquits_per_line.png')

    fig, ax = plt.subplots(figsize=(18, 18))
    ax.bar(parades_amb_mosquits, mosquits_per_parada, color=DEFAULT_COLOR)
    ax.set_ylabel("Quantitat", fontsize=20)
    ax.set_xlabel("Parada", fontsize=20)
    ax.set_title("Alertes de Mosquits per parada ultims " + NUM_DAYS_TO_ANALYZE + " dies", fontsize=24)
    plt.xticks(fontsize=14, rotation=60, ha='right')
    plt.savefig(OUTPUT_FOLDER + '/mosquits_per_station.png')

def total_mosquitos_ranking():
    ranking_count = 3
    moskito_id = synchronize_info['data']['alert_types']['data'][0]['name']
    mosquito_data = [record for record in list_alerts if record['alert_type'] == moskito_id]
    total_count = len(mosquito_data)

    # Count occurrences of station_id
    station_id_counts = Counter(record['station_id'] for record in mosquito_data)

    # Find the three most common station_ids
    top_three_stations = station_id_counts.most_common(ranking_count)

    # Extract weekdays and count their occurrences
    weekdays = [record['date'].strftime('%A') for record in mosquito_data]
    weekday_counts = Counter(weekdays)

    # Find the three most common weekdays
    top_three_weekdays = weekday_counts.most_common(ranking_count)

    # Extract hours and count their occurrences
    hours = [record['date'].hour for record in mosquito_data]
    hour_counts = Counter(hours)

    # Find the three most common hours
    top_three_hours = hour_counts.most_common(ranking_count)

    # Load a font
    title_font = ImageFont.truetype(SCRIPT_PATH + "/font/ranking.ttf", 20)
    section_title_font = ImageFont.truetype(SCRIPT_PATH + "/font/ranking-title.ttf", 30)
    data_font = ImageFont.truetype(SCRIPT_PATH + "/font/ranking.ttf", 20)

    # Space between lines
    line_space = 3
    section_space = 10

    # Text, font, space after
    lines_to_print = [
        (f"Ranking Alertes de Mosquits", title_font, line_space),
        (f"pels últims {NUM_DAYS_TO_ANALYZE} dies", title_font, section_space),
        ("Total Alertes", section_title_font, line_space),
        (str(total_count), data_font, section_space),
        ("Ranking Estacions", section_title_font, line_space),
    ]
    lines_to_print += [(f"{station[0]} ({str(station[1])})", data_font, line_space) for station in top_three_stations]
    lines_to_print += [("Dies amb mes alertes", section_title_font, line_space)]
    lines_to_print += [(f"{weekday[0]} ({str(weekday[1])})", data_font, line_space) for weekday in top_three_weekdays]
    lines_to_print += [("Franja horaria", section_title_font, line_space)]
    lines_to_print += [(f"{hours[0]}h ({str(hours[1])})", data_font, line_space) for hours in top_three_hours]

    # Create a blank image with a white background
    img = Image.new("RGB", (500, 500), color="white")

     # Draw the text
    draw = ImageDraw.Draw(img)

    # Calculate the height needed for the text
    current_height = 0
    for string, font, space in lines_to_print:
        value_width, value_height = draw.textsize(string, font=font)
        current_height += value_height + space

    # Recreate the image with the new height
    img = Image.new("RGB", (500, current_height), color="white")
    draw = ImageDraw.Draw(img)

    # Draw the text on the image
    current_height = 0
    for string, font, space in lines_to_print:
        value_width, value_height = draw.textsize(string, font=font)
        draw.text(((500 - value_width) / 2, current_height), string, fill="black", font=font)
        current_height += value_height + space

    # Save the image
    img_path = OUTPUT_FOLDER + '/ranking.png'
    img.save(img_path)



################################################################################
########################## Executa #############################################
# alertes_per_setmana()
# alertes_per_dia()


alertes_per_dia_de_la_setmana()
alertes_per_hora()
parades_amb_mosquits()
total_mosquitos_ranking()
################################################################################
