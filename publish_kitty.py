import settings
import sys

import settings
from publish import Publisher

if sys.argv.__len__() != 2:
    print('Images folder needed')
    exit(1)

IMAGES_FOLDER = sys.argv[1]

publisher = Publisher(IMAGES_FOLDER)

publisher.publish(tlg_msg=settings.KITTY_MSG, twt_msg=settings.KITTY_MSG)